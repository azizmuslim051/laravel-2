<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    protected $guarded=['id'];

    public function kritiks(){
        return $this->hasMany(Kritik::class);
    }
    public function genre(){
        return $this->belongsTo(Genre::class);
    }
}
