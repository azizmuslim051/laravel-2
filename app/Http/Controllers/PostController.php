<?php

namespace App\Http\Controllers;

use App\Cast;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function __construct(){
        $this->middleware('auth')->except(['index','show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Cast::all();
        return view('posts.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validasi = $request->validate([
           'nama' => 'required',
           'bio' => 'required',
           'umur' => 'required'
        ]);
        Cast::create([
            'nama' => $request->nama,
            'bio' => $request->bio,
            'umur' => $request->umur
        ]);
        return redirect('posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detail = Cast::find($id);
        return view('posts.show',compact('detail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail= Cast::find($id);
        return view('posts.edit',compact('detail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validasi dulu
        $validasi = $request->validate([
            'nama' => 'required|unique:casts',
            'bio' => 'required'
        ]);

        // update dulu
        $update = Cast::find($id);
        $update->nama =$request->nama;
        $update->bio =$request->bio;
        $update->umur =$request->umur;
        $update->update();
        return redirect('/posts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cari = Cast::find($id);
        $cari->delete();
        return redirect('/posts');
    }
}
