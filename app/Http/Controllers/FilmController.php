<?php

namespace App\Http\Controllers;

use App\Film;
use App\Genre;
use Illuminate\Http\Request;

class FilmController extends Controller
{
    public function index(Genre $genre)
    {
        $data = $genre->films;
        return view('film.index',compact('data','genre'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Genre $genre)
    {
        return view('film.create',compact('genre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Genre $genre)
    {
        $validasi = $request->validate([
           'judul' => 'required',
           'ringkasan' => 'required',
           'tahun' => 'required'
        
        ]);
        Film::create([
            'judul' => $request->judul,
            'ringkasan' => $request->ringkasan,
            'tahun' => $request->tahun,
            'genre_id' => $genre->id
            
        ]);
        return redirect()->route('film.index',compact('genre'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Genre $genre, Film $film)
    {
      
        return view('genre.show',compact('detail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Genre $genre, Film $film)
    {
        
        return view('genre.edit',compact('detail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Genre $genre, Film $film)
    {
        // validasi dulu
        $validasi = $request->validate([
            'nama' => 'required|unique:casts',
            
        ]);

        // update dulu
       
        $genre->nama =$request->nama;
        $$genre->update();
        return redirect('/genre');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Genre $genre,Film $film )
    {
       
        $genre->delete();
        return redirect('/genre');
    }

}
