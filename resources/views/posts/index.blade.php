@extends('layouts.master')
@section('nama','ini show data')
@section('content')
@auth
    
<a href="/posts/create" class="btn btn-primary mb-3">Tambah</a>
@endauth
<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Bio</th>
        <th scope="col">Umur</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($data as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->nama}}</td>
                <td>{{$value->bio}}</td>
                <td>{{$value->umur}}</td>
                <td>
                    @auth
                        
                   
                    <form action="/posts/{{$value->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <a href="/posts/{{$value->id}}" class="btn btn-info">Show</a>
                        <a href="/posts/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                    @endauth
                    @guest
                    <a href="/posts/{{$value->id}}" class="btn btn-info">Show</a>
                    @endguest
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection