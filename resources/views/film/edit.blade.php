@extends('layouts.master')
@section('nama','ini edit')
@section('content')    
<div>
    <h2>Edit Post {{$detail->id}}</h2>
    <form action="/posts/{{$detail->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="title">nama</label>
            <input type="text" class="form-control" name="nama" value="{{$detail->nama}}" id="nama" placeholder="Masukkan nama">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">bio</label>
            <input type="text" class="form-control" name="bio"  value="{{$detail->bio}}"  id="bio" placeholder="Masukkan bio">
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">umur</label>
            <input type="text" class="form-control" name="umur"  value="{{$detail->umur}}"  id="umur" placeholder="Masukkan umur">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>
@endsection