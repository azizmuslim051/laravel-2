@extends('layouts.master')
@section('nama','ini index')
@section('content')
<form action="/film/{{$genre->id}}" method="post">
    @csrf
    <div class="form-group">
      <label for="judul">judul</label>
      <input type="text" class="form-control" name="judul" id="judul">  
    </div>
    @error('judul')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
      <label for="ringkasan">ringkasan</label>
      <input type="text" class="form-control" name="ringkasan" id="ringkasan">  
    </div>
    @error('ringkasan')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
      <label for="tahun">tahun</label>
      <input type="text" class="form-control" name="tahun" id="tahun">  
    </div>
    @error('tahun')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror

      
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection