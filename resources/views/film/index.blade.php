@extends('layouts.master')
@section('nama','ini show data')
@section('content')
@auth
    
<a href="/film/{{$genre->id}}/create" class="btn btn-primary mb-3">Tambah</a>
@endauth
<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">judul</th>
        <th scope="col">ringkasan</th>
        <th scope="col">tahun</th>
        <th scope="col">genre</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($data as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->judul}}</td>
                <td>{{$value->ringkasan}}</td>
                <td>{{$value->tahun}}</td>
                <td>{{$value->genre->nama}}</td>
                <td>
                    @auth
                        
                   
                    <form action="/film/{{$genre->id}}/{{$value->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <a href="/film/{{$genre->id}}/{{$value->id}}" class="btn btn-info">Show</a>
                        <a href="/film/{{$genre->id}}/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                    @endauth
                    @guest
                    <a href="/film/{{$value->id}}" class="btn btn-info">Show</a>
                    @endguest
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection