@extends('layouts.master')
@section('nama','ini index')
@section('content')
<form action="/genre" method="post">
    @csrf
    <div class="form-group">
      <label for="nama">nama</label>
      <input type="text" class="form-control" name="nama" id="nama">  
    </div>
    @error('nama')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror

      
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection