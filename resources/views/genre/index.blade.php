@extends('layouts.master')
@section('nama','ini show data')
@section('content')
@auth
    
<a href="/genre/create" class="btn btn-primary mb-3">Tambah</a>
@endauth
<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($data as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->nama}}</td>
                <td>
                    @auth
                        
                   
                    <form action="/genre/{{$value->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <a href="/genre/{{$value->id}}" class="btn btn-info">Show</a>
                        <a href="/genre/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                        <a href="/film/{{$value->id}}/edit" class="btn btn-primary">films</a>
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                    @endauth
                    @guest
                    <a href="/genre/{{$value->id}}" class="btn btn-info">Show</a>
                    @endguest
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection