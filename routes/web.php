<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\PostController;

Auth::routes();

Route::get('/', function () {
    return view('index');
});
Route::get('/data-tables','IndexController@index');
Route::get('/registrasi','IndexController@form');
Route::post('/profile','IndexController@profile');

// posts
Route::get('/posts', 'PostController@index');
Route::get('/posts/create', 'PostController@create')->name('post.create');
Route::post('/posts', 'PostController@store')->name('post.store');
Route::get('/posts/{id}', 'PostController@show')->name('post.show');
Route::get('/posts/{id}/edit', 'PostController@edit')->name('post.edit');
Route::put('/posts/{id}', 'PostController@update')->name('post.update');
Route::delete('/posts/{id}', 'PostController@destroy')->name('post.destroy');
// genre
Route::get('/genre', 'GenreController@index')->name('genre.index');
Route::get('genre/create', 'GenreController@create')->name('genre.create');
Route::post('/genre', 'GenreController@store')->name('genre.store');
Route::get('/{genre}', 'GenreController@show')->name('genre.show');
Route::get('/{genre}/edit', 'GenreController@edit')->name('genre.edit');
Route::put('/{genre}', 'GenreController@update')->name('genre.update');
Route::delete('/{genre}', 'GenreController@destroy')->name('genre.destroy');
// film
Route::get('/film/{genre}', 'FilmController@index')->name('film.index');
Route::get('/{genre}/create', 'FilmController@create')->name('film.create');
Route::post('/{genre}', 'FilmController@store')->name('film.store');
Route::get('/{genre}/{film}', 'FilmController@show')->name('film.show');
Route::get('/{genre}/{film}/edit', 'FilmController@edit')->name('film.edit');
Route::put('/{genre}/{film}', 'FilmController@update')->name('film.update');
Route::delete('/{genre}/{film}', 'FilmController@destroy')->name('film.destroy');




